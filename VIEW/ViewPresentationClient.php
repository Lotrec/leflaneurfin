<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Présentation</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</head>

<header>
    <?php
        include("header.php");
    ?>
</header>

<body>
 
    <?php
        include "../MODEL/model.php";
        include "../MODEL/debug.php";

    $presentations = readAllPresentation();
    ?>

    <?php foreach ($presentations as $presentation) { ?>
        <div>
            <h2><?php echo($presentation['titre']) ?></h2>
            <p><?php echo($presentation['description']) ?></p>
        </div>
    <?php } ?>

</body>

<footer>
    <?php
        include("footer.php");
    ?>
</footer>

</html>
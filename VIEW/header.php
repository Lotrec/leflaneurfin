<!-- CSS MODIFS -->
<link rel="stylesheet" href="main.css">

<nav class="navbar navbar-expand-md bg-body-tertiary">

  <img class="logo-main-img" src="images/logoMain.jpg" alt="Le guide du flâneur, partons près pour aller loin">
  <a class="navbar-brand" href="#"></a>

  <div class="container-fluid border border-info border-rounded back-orange">

    <!-- BOUTON MENU -->
    <button
      class="navbar-toggler"
      type="button"
      data-bs-toggle="collapse"
      data-bs-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- FIN BOUTON MENU -->

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">

<!-- Accueil -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
            href="ViewPresentation.php"
          >
            Accueil
          </a>

          <ul class="dropdown-menu border border-info">
            <li><a class="dropdown-item" href="homepage.php">Recherche</a></li>
          </ul>
        </li>
        <!-- fin accueil-->
 

        <!-- PRESENTATION -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
            href="ViewPresentation.php"
          >
            Présentation
          </a>

          <ul class="dropdown-menu border border-info">
            <li><a class="dropdown-item" href="ViewPresentationClient.php">Présentation</a></li>
            <li><a class="dropdown-item" href="#">Tourisme durable</a></li>
          </ul>
        </li>
        <!-- FIN PRESENTATION -->

        <!-- DECOUVRIR LES REGIONS -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Découvrir nos régions
          </a>

          <ul class="dropdown-menu border border-info">
            <li><a class="dropdown-item" href="#">Région 1</a></li>
            <li><a class="dropdown-item" href="#">Région 2</a></li>
            <li><a class="dropdown-item" href="#">Région 3</a></li>
            <li><a class="dropdown-item" href="#">Région 4</a></li>
            <li><a class="dropdown-item" href="#">Région 5</a></li>
          </ul>
        </li>
        <!-- FIN DECOUVRIR LES REGIONS -->

        <!-- PARCOURS -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Les parcours
          </a>

          <ul class="dropdown-menu border border-info">
            <li><a class="dropdown-item" href="parcour.php">Les balades</a></li>
            <li><a class="dropdown-item" href="#">Les randonnées</a></li>
          </ul>
        </li>
        <!-- FIN PARCOURS -->

        <!-- ACTIVITES ET ACTEURS LOCAUX -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Activités et acteurs locaux
          </a>

          <ul class="dropdown-menu border border-info">
            <li><a class="dropdown-item" href="vuelistactivit.php">Voir tout</a></li>
            <li><a class="dropdown-item" href="artisans.php">Artisans</a></li>
            <li><a class="dropdown-item" href="boutiques.php">Boutiques</a></li>
            <li><a class="dropdown-item" href="restaurants.php">Restaurants</a></li>
            <li><a class="dropdown-item" href="hebergements.php">Hébergements</a></li>
            <li><a class="dropdown-item" href="visites.php">Visites</a></li>
          </ul>
        </li>
        <!-- FIN ACTIVITES ET ACTEURS LOCAUX -->

        <!-- LES ACTUS ET LE BLOG -->
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Les actus et le blog
          </a>

          <ul class="dropdown-menu border border-info">
            <li><a class="dropdown-item" href="#">Actualités locales</a></li>
            <li><a class="dropdown-item" href="#">Le blog du flâneur</a></li>
          </ul>
        </li>
        <!-- FIN LES ACTUS ET LE BLOG -->

        <!-- CONTACT -->
        <li class="nav-item dropdown">
          <a
            class="nav-link"
            href="contact.php"
            role="button"
          >
            Contact
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="viewContactClient.php">Notre contact</a></li>
          </ul>
        </li>
        <!-- FIN CONTACT -->
      </ul>

    </div>
  </div>
</nav>
<!-- CSS MODIFS -->
<link rel="stylesheet" href="main.css">

<ul class="nav justify-content-center border border-info border-rounded back-orange text-blue">

    <!-- NOM  -->
    <li class="nav-item">
        <a class="nav-link disabled">Le guide du flaneur</a>
    </li>

    <!-- PLAN DU SITE -->
    <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="plansite.php">Plan du site</a>
    </li>

    <!-- MENTIONS LEGALES -->
    <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="mentionslegales.php">Mentions légales</a>
    </li>

</ul>
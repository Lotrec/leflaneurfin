<link rel="stylesheet" href="../lstyle.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>

<?php
include_once "../MODEL/crudactivite.php";
include_once "../MODEL/debug.php";
include_once "../MODEL/pdo.php"; 
include "header.php";

$activites = readallactivite();
foreach($activites as $activite){
?>

<div class="resumficheactivite">
        <div class="textcontain">
            <div class="description">
                <img src="" alt="">
                <h2><?= $activite['nom'] ?></h2>
                <p> <?= $activite['description'] ?> </p> 
            </div>
        </div>

            <a href="vueactivclient.php?activiteId=<?= $activite['activiteId'] ?>">Voir plus</a>
            
</div>

<?php
} 
include "footer.php";
?>
<?php
include 'pdo.php';

function createPresentation($titre, $description){
    global $pdo;
    $req = $pdo->prepare("insert into presentation (titre, description) values (?, ?);");
    $req->execute([$titre, $description]);
};

function readPresentation($id){
    global $pdo;
    $req = $pdo->prepare("select * from presentation where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readAllPresentation(){
    global $pdo;
    $req = $pdo->prepare("select * from presentation ;");
    $req->execute(); 
    return $req->fetchAll();
};


function updatePresentation($id, $titre, $description){
    global $pdo;
    $req = $pdo->prepare("update presentation set titre=?, description=? where id=?;");
    $req->execute([$id, $titre, $description]);
};

function deletePresentation($id){
    global $pdo;
    $req = $pdo->prepare("delete from presentation where id=?;");
    $req->execute([$id]);
};

function createContact($titre, $mail, $numero){;
    global $pdo;
    $req = $pdo->prepare("insert into contact (titre, mail, numero) values (?, ?, ?);");
    $req->execute([$titre, $mail, $numero]);
};

function updateContact($id, $titre, $mail, $numero){;
    global $pdo;
    $req = $pdo->prepare("update contact set titre=?, mail=?, numero=? where id=?;");
    $req->execute([$id, $titre, $mail, $numero]);
};

function readContact($id){
    global $pdo;
    $req = $pdo->prepare("select * from contact where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function readAllContact(){
    global $pdo;
    $req = $pdo->prepare("select * from contact ;");
    $req->execute(); 
    return $req->fetchAll();
};

function deleteContact($id){
    global $pdo;
    $req = $pdo->prepare("delete from contact where id=?;");
    $req->execute([$id]);
};

function createparcour($nom, $description, $desc_detaillee1, $desc_detaillee2, $visible, $code_postal, $lat, $lng, $durée, $imgURL){
    global $pdo;
    $req1 = $pdo->prepare('insert into parcours (nom, description, desc_detaillee1, desc_detaillee2, visible, code_postal, lat, lng, durée) values (?, ?, ?, ?, ?, ?, ?, ?, ?);');
    $req1->execute([$nom, $description, $desc_detaillee1, $desc_detaillee2, $visible, $code_postal, $lat, $lng, $durée]);
    $id_parcours = $pdo->lastInsertId();
    $req2 = $pdo->prepare('insert into parcours_img (id_parcours, imgURL) values (?, ?);');
    $req2->execute([$id_parcours, $imgURL]);
  }

  function deleteparcour($id){
    global $pdo;
    $req = $pdo->prepare("delete from parcours where activiteId=?;");
    $req->execute([$id]);
};
  
  
  function getParcour($id){
    global $pdo;
    // select * from parcour where id = $id
    // select * from parcours_img where id_parcours = $id
    // retourner les deux
    $req1 = $pdo->prepare("select * from parcours where parcoursId = ?;");
    $req1->execute([$id]);
    $req2 = $pdo->prepare("select * from parcours_img where id_parcours = ?;");
    $req2->execute([$id]);
    return $req =["parcours" => $req1->fetch(), "images" => $req2->fetchAll()];
    }
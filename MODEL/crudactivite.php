
<?php
include_once "debug.php";
include_once "pdo.php"; 


function createactivite($categorie, $nom, $adresse, $code_postal, $lat, $lng, $sous_titre, $description, $sous_titre2, $description2, $sous_titre3, $description3, $sous_titre4, $description4,
$gratuit, $visible, $telephone, $mail, $reseau1, $reseau2, $reseau3, $lien_map){ 
    global $pdo;
    $req = $pdo->prepare("insert into activite(categorie, nom, adresse, code_postal, lat, lng, sous_titre, description, sous_titre2, description2, sous_titre3, description3, sous_titre4, description4, gratuit, visible, telephone, mail, reseau1, reseau2, reseau3, lien_map) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
    $req->execute([$categorie, $nom, $adresse, $code_postal, $lat, $lng, $sous_titre, $description, $sous_titre2, $description2, $sous_titre3, $description3, $sous_titre4, $description4,
    $gratuit, $visible, $telephone, $mail, $reseau1, $reseau2, $reseau3, $lien_map]);
};


function readallactivite(){
    global $pdo;
    $req = $pdo->query("select * from activite");
    return $req->fetchAll();
};


function readactivite($id){
    global $pdo;
    $req = $pdo->prepare("select * from activite where activiteId=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};



function updateactivite($id, $categorie, $nom, $adresse, $code_postal, $lat, $lng, $sous_titre, $description, $sous_titre2, $description2, $sous_titre3, $description3, $sous_titre4, $description4,
$gratuit, $visible, $telephone, $mail, $reseau1, $reseau2, $reseau3, $lien_map ){
    // on met l'id en premiere car c'est lui qu'on utilise pour identifier l'activité
    global $pdo; 
    $req = $pdo->prepare("update activite set categorie=?, nom=?, adresse=?, code_postal=?, lat=?, lng=?, sous_titre=?, description=?, sous_titre2=?, description2=?,sous_titre3=?, description3=?,sous_titre4=?, description4=?, gratuit=?, visible=?, telephone=?, mail=?, reseau1=?, reseau2=?, reseau3=?, lien_map=? where activiteId=?;");
    //on met l'id en dernier car on ne le modifie pas
    $req->execute([$categorie, $nom, $adresse, $code_postal, $lat, $lng, $sous_titre, $description, $sous_titre2, $description2, $sous_titre3, $description3, $sous_titre4, $description4,
    $gratuit, $visible, $telephone, $mail, $reseau1, $reseau2, $reseau3, $lien_map, $id]);
};
    

function deleteactivite($id){
    global $pdo;
    $req = $pdo->prepare("delete from activite where activiteId=?;");
    $req->execute([$id]);
};

function getActivite($id){
    global $pdo;
    // select * from activite where id = $id
    // select * from activite_img where id_activite = $id
    // retourner les deux
    $req1 = $pdo->prepare("select * from activite where activiteId = ?;");
    $req1->execute([$id]);
    $req2 = $pdo->prepare("select * from activite_img where id_activite = ?;");
    $req2->execute([$id]);
    return $req =["activite" => $req1->fetch(), "images" => $req2->fetchAll()];
    }


?>
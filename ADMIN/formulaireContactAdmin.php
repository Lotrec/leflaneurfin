<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification contact</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="main.css">
</head>
<header>
    <?php
        include("../VIEW/header.php");
    ?>
</header>
<body>
<?php
    include "../MODEL/model.php";
    include "../MODEL/debug.php";
    ?>

<form action="../CONTROL/createContact.php" method="post">
    <label for="titre">Titre :</label>
    <input type="text" id="titre" name="titre" required><br>

    <label for="mail">mail :</label>
    <input type="text" id="mail" name="mail" required><br>

    <label for="numero">numéro de téléphone :</label>
    <input type="number" id="numero" name="numero" required><br>

    <input type="submit" value="créer">
</form>

</body>

<footer>
    <?php
        include("../VIEW/footer.php");
    ?>
</footer>

</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<header>
    <?php
        include("../VIEW/header.php");
    ?>
</header>

<body>
    <?php
    include "../MODEL/model.php";
    include "../MODEL/debug.php";
    ?>

<form action="../CONTROL/createPresentation.php" method="post">
    <label for="titre">Titre :</label>
    <input type="text" id="titre" name="titre"><br>

    <label for="description">Description :</label>
    <textarea id="description" name="description" rows="4" cols="50"></textarea>

    <input type="submit" value="Créer la présentation">
</form>

</body>

<footer>
    <?php
        include("../VIEW/footer.php");
    ?>
</footer>

</html>
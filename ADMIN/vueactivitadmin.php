<!--Affiche la liste des activités pour l'admin-->
<link rel="stylesheet" href="lstyle.css">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des activités</title>
    <link rel="stylesheet" href="../lstyle.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
</head>

<header>
    <?php
        include("../VIEW/header.php");
    ?>
</header>

<body>
    
    <?php
include_once "../MODEL/crudactivite.php";
include_once "../MODEL/debug.php";
include_once "../MODEL/pdo.php";
?>

<a href="formcreateactivite.php">Créer une nouvelle activité</a>

<?php
$activites = readallactivite();
foreach($activites as $activite){
?>

<div class="resumficheactivite">
    <div class="textcontain">
            <div class="description">
                <?= $activite['activiteId'] ?>
                <h2><?= $activite['nom'] ?></h2>
                <p> <?= $activite['description'] ?> </p> 
            </div>
        </div>

            <a href="../CONTROL/deleteactiv.php?activiteId=<?= $activite['activiteId'] ?>">Supprimer l'activité</a>
            <a href="formupdateactiv.php?activiteId=<?= $activite['activiteId'] ?>">Modifier l'activité</a>
</div>

<?php
} 
?>
</body>

<footer>
    <?php
        include("../VIEW/footer.php");
    ?>
</footer>

</html>





<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification des activités</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</head>

<header>
    <?php
        include("../VIEW/header.php");
    ?>
</header>

<body>
<?php
include_once "../MODEL/pdo.php";
include_once "../MODEL/crudactivite.php";

$req = $pdo->query("select * from activite where activiteId={$_GET['activiteId']};");
$activites = $req->fetchAll();

foreach($activites as $activite){
 ?>  

<form action = "../CONTROL/updateactiv.php" method= "post"> 
    <div class="nomActivite">
        <input type="hidden"     name="activiteId"      value="<?= $activite['activiteId']?>">
        <input type= "text"      name="categorie"       value="<?= $activite['categorie'] ?>"> 
        <input type= "text"      name="nom"             value="<?= $activite['nom'] ?>"> 
    </div>
    <div class="adresseActivite">
        <input type= "text"      name="adresse"         value="<?= $activite['adresse'] ?>"> 
        <input type= "number"    name="code_postal"     value="<?= $activite['code_postal'] ?>"> 
        <input type= "number"    name="lat"             value="<?= $activite['lat'] ?>"> 
        <input type= "number"    name="lng"             value="<?= $activite['lng'] ?>">
    </div>
    <div class="lesDescriptionActivite">
        <div class="descriptionObligatoire">
            <input type= "text"      name="sous_titre"      value="<?= $activite['sous_titre'] ?>"> 
            <textarea class="form-control" name="description" id="description" rows="10" value="<?= $activite['description'] ?>"></textarea>
        </div>
            <input type= "text"      name="sous_titre2"      value="<?= $activite['sous_titre2'] ?>"> 
            <textarea class="form-control" name="description2" id="description2" rows="10" value="<?= $activite['description2'] ?>"></textarea>
            <input type= "text"      name="sous_titre3"      value="<?= $activite['sous_titre3'] ?>"> 
            <textarea class="form-control" name="description3" id="description3" rows="10" value="<?= $activite['description3'] ?>"></textarea>
            <input type= "text"      name="sous_titre4"      value="<?= $activite['sous_titre4'] ?>"> 
            <textarea class="form-control" name="description4" id="description4" rows="10" value="<?= $activite['description4'] ?>"></textarea>
    </div>

        <input type= "boolean"   name="gratuit"         value="<?= $activite['gratuit'] ?>"> 
        <input type= "boolean"   name="visible"         value="<?= $activite['visible'] ?>"> 
    <div class="contactActivite">
        <input type= "text"      name="telephone"       value="<?= $activite['telephone'] ?>"> 
        <input type= "text"      name="mail"            value="<?= $activite['mail'] ?>"> 
        <input type= "text"      name="reseau1"         value="<?= $activite['reseau1'] ?>"> 
        <input type= "text"      name="reseau2"         value="<?= $activite['reseau2'] ?>"> 
        <input type= "text"      name="reseau3"         value="<?= $activite['reseau3'] ?>"> 
        <input type= "text"      name="lien_map"        value="<?= $activite['lien_map'] ?>"> 
    </div>   

    <input type="submit"     value="modifier">
</form>
<?php } ?>

</body>

<footer>
    <?php
        include("../VIEW/footer.php");
    ?>
</footer>

</html>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification de la page</title>
</head>

<header>
    <?php
        include("../VIEW/header.php");
    ?>
</header>

<body>

  <?php
      include "../MODEL/model.php";
      include "../MODEL/debug.php";
      ?>

  <form action="../CONTROL/createPresentation.php" method="post">

    <label for="id">ID de la présentation :</label>
    <input type="text" name="id" id="id" ><br>

    <label for="titre">Titre :</label>
    <input type="text" name="titre" id="titre" ><br>

    <label for="description">Description :</label>
    <textarea name="description" id="description" ></textarea><br>

    <input type="submit" value="Mettre à jour">
  </form>

</body>

<footer>
    <?php
        include("../VIEW/footer.php");
    ?>
</footer>

</html>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire de création des activités</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</head>

<header>
    <?php
        include("../VIEW/header.php");
    ?>
</header>
<body>
 <link rel="stylesheet" href="lstyle.css">

<?php
include_once "../MODEL/pdo.php";
include_once "../MODEL/crudactivite.php";
include "../MODEL/debug.php";
?>


<form action ="../CONTROL/creatactiv.php" method ="post"> 

    <input type="hidden" name="activiteId">
   
    <input type="radio" id="restaurant" name="categorie" value="resto">
    <label for="restaurant">restaurant</label>

    <input type="radio" id="hebergement" name="categorie" value="hebergement">
    <label for="hebergement">hebergement</label>

    <input type="radio" id="Visites" name="categorie" value="visite">
    <label for="visite">Visites</label>
    
    <input type="radio" id="rando" name="categorie" value="rando">
    <label for="rando">rando</label>

    <input type="radio" id="boutique" name="categorie" value="boutique">
    <label for="boutique">boutique</label>

    <input type="radio" id="artisan" name="categorie" value="artisan">
    <label for="artisan">artisan</label>
 

    </br>
    <input type= "boolean"  name="gratuit"   placeholder="si gratuit ecrire 1, sinon écrire 0">
    <input type= "boolean"  name="visible"   placeholder="si visible ecrire 1, sinon écrire 0">


    <div class ="containeractivclient">
        <div class = "hautdepage">
            <h2> <input type= "text"      name="nom"     placeholder="nom de l'activité/producteurs">  </h2>
        </div>

        <div class="textpresentationactivite"   >
            <h3> <input type= "text"      name="sous_titre"   placeholder=" Titre accrocheur"></h3>
            <p> <textarea class="form-control" name="description" id="description" rows="10" placeholder="description de l'activité"></textarea></p>
        </div> 

        <!-- <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <input type= "url"       name="image"   placeholder="image url">
                </div>

                <div class="carousel-item">
                    <input type= "url"       name="image"   placeholder="image url">
                </div>

                <div class="carousel-item">
                    <input type= "url"       name="image"   placeholder="image url">
                </div>
            </div>
        </div> -->

        <div class="textpresentationactivite"   >
            <h3> <input type= "text"      name="sous_titre2"   placeholder=" Titre accrocheur"></h3>
            <p> <textarea class="form-control" name="description2" id="description2" rows="10" placeholder="description de l'activité"></textarea></p>
        </div> 

        <div class="textpresentationactivite"   >
            <h3> <input type= "text"      name="sous_titre3"   placeholder=" Titre accrocheur"></h3>
            <p> <textarea class="form-control" name="description3" id="description3" rows="10" placeholder="description de l'activité"></textarea></p>
        </div>   
        
        <div class="textpresentationactivite"   >
            <h3> <input type= "text"      name="sous_titre4"   placeholder=" Titre accrocheur"></h3>
            <p> <textarea class="form-control" name="description4" id="description4" rows="10" placeholder="description de l'activité"></textarea> </p>
        </div> 


        <div class="contact">
            <h3>Contacts</h3>  
            <p> <input type= "text"      name="adresse"    placeholder="adresse"> </p>   
            <p> <input type= "number"    name="code_postal"         placeholder="code postale"> </p>
            <p> <input type= "text"      name="lat"        placeholder="latitude"> </p>
            <p> <input type= "text"      name="lng"        placeholder="longitude"> </p>
            <p> <input type= "text"      name="telephone"  placeholder="telephone"> </p>
            <p> <input type= "mail"      name="email"      placeholder="email"></p>
            <p> <input type= "url"       name="reseau1"    placeholder="Facebook"></p>
            <p> <input type= "url"       name="reseau2"    placeholder="Instagram"></p>
            <p> <input type= "url"       name="reseau3"    placeholder="youtube ou autres"></p>
        </div>
    </div>  
    <input type="submit" value="Créer cette activité">
</form>

</body>

<footer>
    <?php
        include("../VIEW/footer.php");
    ?>
</footer>

</html>